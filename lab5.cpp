//Cameron Goldsmith
//ctgolds
//CPSC 1020 Spring 2019
//Section 004
//lab 5


#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/

   Card deckArray[52];

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

   int c = 0;
   for(int i =0; i < 4; i++){
   for(int x = 2;x<15;x++){
     deckArray[c].value = x;
     deckArray[c].suit = static_cast<Suit>(i);
      c++;
   }
   }

  random_shuffle(&deckArray[0], &deckArray[52] ,myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/

    Card handofcards[5] = {deckArray[0], deckArray[1], deckArray[2], deckArray[3], deckArray[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
      sort(handofcards,handofcards+5, suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */

   for(int output = 0; output < 5; ++output){


   cout << setw(10) << right << get_card_name(handofcards[output])<< get_suit_code(handofcards[output]) << endl;
    }

  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT

     if (lhs.suit < rhs.suit) {
     return true;
     }

     else if( lhs.suit == rhs.suit){
        return (lhs.suit < rhs.suit);
        }
         return false;
   }




string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENT

 switch(c.value)
 {
   case 2: return "2 of ";
   case 3: return "3 of ";
   case 4: return "4 of ";
   case 5: return "5 of ";
   case 6: return "6 of ";
   case 7: return "7 of ";
   case 8: return "8 of ";
   case 9: return "9 of ";
   case 10: return "10 of ";
   case 11: return "Jack of ";
   case 12: return "Queen of ";
   case 13: return "King of ";
   case 14: return "Ace of ";

default: return "";
 }
 //to_sting(value);

}
